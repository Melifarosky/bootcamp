-- DROP DATABASE tietodb;
-- CREATE DATABASE tietodb;

CREATE TABLE restaurant (
	id serial NOT NULL PRIMARY KEY,
	name varchar(128) NOT NULL,
	address varchar(256) NOT NULL,
	"timestamp" timestamp DEFAULT localtimestamp(0),
	CONSTRAINT ck_restaurant_name_not_empty CHECK (TRIM(name) != '' AND name !~ '^[[:digit:]]+$'),
	CONSTRAINT ck_restaurant_address_not_empty CHECK (TRIM(address) != '' AND address !~ '^[[:digit:]]+$')
);

CREATE TABLE score (
	id serial NOT NULL PRIMARY KEY,
	restaurant_id integer NOT NULL REFERENCES restaurant(id) ON DELETE CASCADE,
	name varchar(64) NOT NULL,
	customer_comment varchar(512) NOT NULL,
	score_value smallint NOT NULL,
	"timestamp" timestamp DEFAULT localtimestamp(0),
	CONSTRAINT ck_score_name CHECK (TRIM(name) != '' AND name !~ '^[[:digit:]]+$'),
	CONSTRAINT ck_score_customer_comment CHECK (TRIM(customer_comment) != '' AND customer_comment !~ '^[[:digit:]]+$'),
	CONSTRAINT ck_score_score_value CHECK (score_value BETWEEN 1 AND 10)
);

CREATE TABLE account (
	id serial NOT NULL PRIMARY KEY,
	name varchar(64) NOT NULL,
	password varchar(64) NOT NULL,
	admin boolean DEFAULT false,
	"timestamp" timestamp DEFAULT localtimestamp(0),
	CONSTRAINT ck_account_name_not_empty CHECK (TRIM(name) != '' AND name !~ '^[[:digit:]]+$')
);

CREATE UNIQUE INDEX ON restaurant (lower(name));
CREATE INDEX ON score (restaurant_id);
CREATE UNIQUE INDEX ON account (name);

INSERT INTO restaurant (name, address) VALUES ('McDonalds', 'Sõpruse pst');
INSERT INTO restaurant (name, address) VALUES ('Vapiano Solaris', 'Estonia pst');
INSERT INTO restaurant (name, address) VALUES ('Reval Cafe', 'Pärnu mnt');
INSERT INTO restaurant (name, address) VALUES ('Peetri pitsa', 'Liivalaia');
INSERT INTO restaurant (name, address) VALUES ('Restoran Tšaikovski', 'Vene tn');
INSERT INTO restaurant (name, address) VALUES ('Hesburger', 'Viru tn');
INSERT INTO restaurant (name, address) VALUES ('Sohver', 'Pärnu mnt');

INSERT INTO score (restaurant_id, name, customer_comment, score_value) VALUES (1, 'Alex Alex', 'Nice place', 9);
INSERT INTO score (restaurant_id, name, customer_comment, score_value) VALUES (1, 'Smith', 'Terrify', 2);

INSERT INTO account (name, password) VALUES ('admin', 'admin123');

CREATE OR REPLACE VIEW restaurant_score_view WITH (security_barrier) AS
SELECT restaurant.*, round(avg(score_value), 1) AS avg_score, count(score.restaurant_id) AS count_score
FROM restaurant
	LEFT JOIN score ON score.restaurant_id = restaurant.id
GROUP BY restaurant.id
ORDER BY restaurant.name
;