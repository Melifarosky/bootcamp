<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../fragments/head.jsp"/>
<body>
<jsp:include page="../fragments/menu.jsp"/>
<div id="body">
    <!-- Main page content -->

    <section class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="alert alert-danger" role="alert" style="display: none;">
                    <span></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2">
                <h1>Administreerimine > Muuda asutus</h1>
            </div>
        </div>
        <div class="row">
            <form:form commandName="restaurant" class="form-horizontal restaurant-validator form-validator" method="post">
                <fieldset>
                    <div class="form-group" >
                        <label for="name" class="col-md-1 col-md-offset-2 control-label">Nimi</label>
                        <div class="col-md-4 col-md-offset-1">
                            <form:input path="restaurantName" name="restaurantName" type="text" class="form-control" id="name" placeholder="Nimi"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-md-1 col-md-offset-2 control-label">Aadress</label>
                        <div class="col-md-4 col-md-offset-1">
                            <form:input path="restaurantAddress" name="restaurantAddress" class="form-control" id="address" placeholder="Aadress" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-6 col-md-2 ">
                            <a class="btn btn-default" href="<c:url value="/admin"/>">< Tagasi</a>
                            <button type="submit" class="btn btn-success" style="margin-left: 5px;">Salvesta</button>
                        </div>
                    </div>
                </fieldset>
            </form:form>
        </div>
    </section>
</div>
<jsp:include page="../fragments/footer.jsp"/>
</body>
</html>
