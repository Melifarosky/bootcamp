<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../fragments/head.jsp"/>
<body>
<jsp:include page="../fragments/menu.jsp"/>
<div id="body">
    <!-- Main page content -->

    <section class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="alert alert-danger" role="alert" style="display: none;">
                    <span></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2">
                <h1>Hinda kohta > ${restaurant.restaurantName}</h1>
            </div>
        </div>
        <div class="row">
            <form:form commandName="score" class="form-horizontal score-validator form-validator" method="post">
                <fieldset>
                    <div class="form-group" >
                        <label for="name" class="col-md-1 col-md-offset-2 control-label">Nimi</label>
                        <div class="col-md-4 col-md-offset-1">
                            <form:input path="customerName" name="customerName" type="text" class="form-control" id="name" placeholder="Nimi"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="comment" class="col-md-1 col-md-offset-2 control-label">Kommentaar</label>
                        <div class="col-md-4 col-md-offset-1">
                            <form:textarea path="customerComment" class="form-control" rows="10" id="comment" placeholder="Kommentaar" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="scores" class="col-md-1 col-md-offset-2 control-label">Hinnang</label>
                        <div id="scores" class="col-md-4 col-md-offset-1">
                            <form:radiobuttons path="customerScore" items="${scoreVals}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-6 col-md-2 ">
                            <a class="btn btn-default" href="/">< Tagasi</a>
                            <button type="submit" class="btn btn-success" style="margin-left: 5px;">Saada!</button>
                        </div>
                    </div>
                </fieldset>
            </form:form>
        </div>

        <div class="row">
            <div class="col-sm-offset-2">
                <h2>Teiste hinnangud</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 col-sm-offset-2" style="padding: 0;">
                <c:forEach items="${scores}" var="score">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">${score.customerName} (${score.customerScore} / 10)</h3>
                            <c:if test="${admin}">
                                <form action="/restaurant/${restaurant.id}/score/delete/${score.id}" method="post" style="margin-left: auto;">
                                    <button class="btn remove-score-btn"><span class="glyphicon glyphicon-remove"></span></button>
                                </form>
                            </c:if>
                        </div>
                        <div class="panel-body">
                            ${score.customerComment}
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </section>
</div>
<jsp:include page="../fragments/footer.jsp"/>
</body>
</html>
