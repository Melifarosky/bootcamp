package com.chapaev.service.score;

import com.chapaev.model.Score;

import java.util.List;

/**
 * Created by Chapaev on 09.06.2017.
 */
public interface ScoreService {
    void addScore(Score score);
    List<Score> getRestaurantScores(Long id);
    void removeScore(Long id);
}
