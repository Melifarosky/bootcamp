package com.chapaev.service.score;

import com.chapaev.dao.score.ScoreDao;
import com.chapaev.model.Score;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Chapaev on 09.06.2017.
 */
@Service
public class ScoreServiceImpl implements ScoreService {
    @Autowired
    private ScoreDao scoreDao;

    @Override
    @Transactional
    public void addScore(Score score) {
        scoreDao.addScore(score);
    }

    @Override
    @Transactional
    public List<Score> getRestaurantScores(Long id) {
        return scoreDao.getRestaurantScores(id);
    }

    @Override
    @Transactional
    public void removeScore(Long id) {
        scoreDao.removeScore(id);
    }
}
