package com.chapaev.service.account;

/**
 * Created by Chapaev on 11.06.2017.
 */
public interface AccountService {
    boolean isCredentialsValid(String name, String password);
}
