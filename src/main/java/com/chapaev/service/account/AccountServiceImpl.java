package com.chapaev.service.account;

import com.chapaev.dao.account.AccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Chapaev on 11.06.2017.
 */
@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountDao accountDao;

    @Override
    @Transactional
    public boolean isCredentialsValid(String name, String password) {
        return accountDao.isCredentialsValid(name, password);
    }
}
