package com.chapaev.service.restaurant;

import com.chapaev.dao.restaurant.RestaurantDao;
import com.chapaev.model.Restaurant;
import com.chapaev.model.RestaurantView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Chapaev on 09.06.2017.
 */
@Service
public class RestaurantServiceImpl implements RestaurantService {
    @Autowired
    private RestaurantDao restaurantDao;

    @Override
    @Transactional
    public void addRestaurant(Restaurant restaurant) {
        restaurantDao.addRestaurant(restaurant);
    }

    @Override
    @Transactional
    public List<Restaurant> getRestaurants() {
        return restaurantDao.getRestaurants();
    }

    @Override
    @Transactional
    public List<RestaurantView> getRestaurantsView() { return restaurantDao.getRestaurantsView(); }

    @Override
    @Transactional
    public Restaurant getRestaurant(Long id) {
        return restaurantDao.getRestaurant(id);
    }

    @Override
    @Transactional
    public void updateRestaurant(Restaurant restaurant) {
        restaurantDao.updateRestaurant(restaurant);
    }

    @Override
    @Transactional
    public void removeRestaurant(Long id) {
        restaurantDao.removeRestaurant(id);
    }
}
