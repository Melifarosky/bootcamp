package com.chapaev.dao.score;

import com.chapaev.model.Score;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Chapaev on 09.06.2017.
 */
@Repository
public class ScoreDaoImpl implements ScoreDao {
    private static final Logger logger = LoggerFactory.getLogger(ScoreDaoImpl.class);

    @Resource(name="localSessionFactoryBean")
    private SessionFactory sessionFactory;

    @Override
    public void addScore(Score score) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(score);
        logger.info("Score record saved successfully" + score);
    }

    @Override
    public List<Score> getRestaurantScores(Long id) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Score> scoreList = session.createCriteria(Score.class)
                .add(Restrictions.eq("restaurantId", id))
                .addOrder(Order.desc("timestamp"))
                .list();

        return scoreList;
    }

    @Override
    public void removeScore(Long id) {
        Session session = this.sessionFactory.getCurrentSession();
        Score score = (Score) session.get(Score.class, id);
        if (score != null) {
            session.delete(score);
            logger.info("Score removed successfully, details = " + score);
        } else
            logger.info("With this ID = " + id + " score hasn't been found!");
    }
}
