package com.chapaev.dao.account;

import com.chapaev.model.Account;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Chapaev on 11.06.2017.
 */
@Repository
public class AccountDaoImpl implements AccountDao {
    private static final Logger logger = LoggerFactory.getLogger(AccountDaoImpl.class);

    @Resource(name="localSessionFactoryBean")
    private SessionFactory sessionFactory;

    @Override
    public boolean isCredentialsValid(String name, String password) {
        Session session = this.sessionFactory.getCurrentSession();
        // As I remember, PGSQL is using by default escape characters, so we don't use it manually.
        List<Account> accountList = session.createCriteria(Account.class)
                .add(Restrictions.eq("name", name))
                .add(Restrictions.eq("password", password))
                .list();

        return isAccountValid(accountList);
    }

    private boolean isAccountValid(List<Account> accountList) {
        return accountList.size() != 0;
    }
}
