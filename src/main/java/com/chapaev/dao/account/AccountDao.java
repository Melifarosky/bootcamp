package com.chapaev.dao.account;

/**
 * Created by Chapaev on 11.06.2017.
 */
public interface AccountDao {
    boolean isCredentialsValid(String name, String password);
}
