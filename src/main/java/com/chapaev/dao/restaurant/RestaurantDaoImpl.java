package com.chapaev.dao.restaurant;

import com.chapaev.model.Restaurant;
import com.chapaev.model.RestaurantView;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Chapaev on 09.06.2017.
 */
@Repository
public class RestaurantDaoImpl implements RestaurantDao {
    private static final Logger logger = LoggerFactory.getLogger(RestaurantDaoImpl.class);

    @Resource(name="localSessionFactoryBean")
    private SessionFactory sessionFactory;

    @Override
    public void addRestaurant(Restaurant restaurant) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(restaurant);
        logger.info("Restaurant record saved successfully");
    }

    @Override
    public List<Restaurant> getRestaurants() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Restaurant> restaurantList = session.createCriteria(Restaurant.class)
                //.add(Restrictions.ge("beginTime", new Date().getTime()))
                .addOrder(Order.asc("restaurantName"))
                .list();

        for (Restaurant restaurant : restaurantList) {
            logger.info("Restaurant List::" + restaurant);
        }

        return restaurantList;
    }

    @Override
    public List<RestaurantView> getRestaurantsView() {
        Session session = this.sessionFactory.getCurrentSession();
        List<RestaurantView> restoranList = session.createCriteria(RestaurantView.class)
                .list();

        for (RestaurantView restoran : restoranList) {
            logger.info("Restaurant View List::" + restoran);
        }

        return restoranList;
    }

    @Override
    public Restaurant getRestaurant(Long id) {
        Session session = this.sessionFactory.getCurrentSession();
        Restaurant restaurant = (Restaurant) session.load(Restaurant.class, id);
        logger.info("Restaurant loaded successfully, Restaurant details=" + restaurant);
        return restaurant;
    }

    @Override
    public void updateRestaurant(Restaurant restaurant) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(restaurant);
        logger.info("Restaurant record updated successfully, Restaurant Details=" + restaurant);
    }

    @Override
    public void removeRestaurant(Long id) {
        Session session = this.sessionFactory.getCurrentSession();
        session.delete(getRestaurant(id));
        logger.info("Restaurant removed successfully!");
    }
}
