package com.chapaev.dao.restaurant;

import com.chapaev.model.Restaurant;
import com.chapaev.model.RestaurantView;

import java.util.List;

/**
 * Created by Chapaev on 09.06.2017.
 */
public interface RestaurantDao {
    void addRestaurant(Restaurant restaurant);
    List<Restaurant> getRestaurants();
    List<RestaurantView> getRestaurantsView();
    Restaurant getRestaurant(Long id);
    void updateRestaurant(Restaurant restaurant);
    void removeRestaurant(Long id);
}
