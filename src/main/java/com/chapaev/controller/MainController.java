package com.chapaev.controller;

import com.chapaev.model.Account;
import com.chapaev.model.Restaurant;
import com.chapaev.model.RestaurantView;
import com.chapaev.model.Score;
import com.chapaev.service.account.AccountService;
import com.chapaev.service.restaurant.RestaurantService;
import com.chapaev.service.score.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Chapaev on 09.06.2017.
 */
@Controller
public class MainController {
    private List<Integer> authUsers = new ArrayList<Integer>();
    private final RestaurantService restaurantService;
    private final ScoreService scoreService;
    private final AccountService accountService;

    @Autowired
    public MainController(RestaurantService restaurantService, ScoreService scoreService, AccountService accountService) {
        this.restaurantService = restaurantService;
        this.scoreService = scoreService;
        this.accountService = accountService;
    }

    @RequestMapping("/")
    public String main() {
        return "redirect:/restaurants";
    }

    @RequestMapping(value = "/restaurants", method = RequestMethod.GET)
    public String restaurants(Model model) {
        List<RestaurantView> restaurantList = restaurantService.getRestaurantsView();
        model.addAttribute("restaurants", restaurantList);
        return "restaurants";
    }

    @RequestMapping(value = "/add/{id}", method = RequestMethod.GET)
    public String newScore(Model model, @PathVariable Long id, HttpSession httpSession) {
        List<Score> scoreList = scoreService.getRestaurantScores(id);
        model.addAttribute("restaurant", restaurantService.getRestaurant((long) id));
        model.addAttribute("scores", scoreList);
        model.addAttribute("score", new Score());
        model.addAttribute("scoreVals", new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)));
        model.addAttribute("admin", isAdminLogged(httpSession));
        return "newScore";
    }

    @RequestMapping(value = "/add/{id}", method = RequestMethod.POST)
    public String newScoreAction(Score score, @PathVariable Long id) {
        score.setRestaurantId(id);
        scoreService.addScore(score);
        return "redirect:/";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String admin(Model model, @RequestParam(value = "auth", required = false, defaultValue = "true") boolean auth, HttpSession httpSession) {
        if (!isAdminLogged(httpSession)) {
            if (!auth) return "redirect:/";
            return "redirect:/admin/login";
        }

        model.addAttribute("restaurants", restaurantService.getRestaurantsView());
        model.addAttribute("admin", isAdminLogged(httpSession));
        return "restaurants";
    }

    @RequestMapping(value = "/admin/login", method = RequestMethod.GET)
    public String adminLogin(Model model) {
        model.addAttribute("account", new Account());
        return "adminLogin";
    }

    @RequestMapping(value = "/admin/login", method = RequestMethod.POST)
    public String adminLoginAction(Account account, HttpSession httpSession) {
        if (accountService.isCredentialsValid(account.getName(), account.getPassword())) {
            System.out.println("Logged successfully!");
            httpSession.setAttribute("loggedAdmin", true);
        } else {
            System.out.println("Login failed!");
            httpSession.setAttribute("loggedAdmin", false);
        }

        return "redirect:/admin";
    }

    @RequestMapping(value = "/restaurant/new", method = RequestMethod.GET)
    public String restaurantNew(Model model, HttpSession httpSession) {
        if (!isAdminLogged(httpSession)) return "redirect:/admin";

        model.addAttribute("restaurant", new Restaurant());
        return "newRestaurant";
    }

    @RequestMapping(value = "/restaurant/new", method = RequestMethod.POST)
    public String restaurantNewAction(Restaurant restaurant, HttpSession httpSession) {
        if (!isAdminLogged(httpSession)) return "redirect:/admin";

        restaurantService.addRestaurant(restaurant);
        return "redirect:/admin";
    }

    @RequestMapping(value = "/restaurant/edit/{id}", method = RequestMethod.GET)
    public String restaurantEdit(Model model, @PathVariable Long id, HttpSession httpSession) {
        if (!isAdminLogged(httpSession)) return "redirect:/admin";

        model.addAttribute("restaurant", restaurantService.getRestaurant(id));
        return "editRestaurant";
    }

    @RequestMapping(value = "/restaurant/edit/{id}", method = RequestMethod.POST)
    public String restaurantEditAction(Restaurant restaurant, @PathVariable Long id, HttpSession httpSession) {
        if (!isAdminLogged(httpSession)) return "redirect:/admin";

        restaurantService.updateRestaurant(restaurant);
        return "redirect:/admin";
    }

    @RequestMapping(value = "/restaurant/delete/{id}", method = RequestMethod.POST)
    public String restaurantDelete(@PathVariable Long id, HttpSession httpSession) {
        if (!isAdminLogged(httpSession)) return "redirect:/admin";

       restaurantService.removeRestaurant(id);
       return "redirect:/admin";
    }

    @RequestMapping(value = "/restaurant/{rId}/score/delete/{id}", method = RequestMethod.POST)
    public String scoreDelete(@PathVariable Long rId, @PathVariable Long id, HttpSession httpSession) {
        if (!isAdminLogged(httpSession)) return "redirect:/admin";

        scoreService.removeScore(id);
        return "redirect:/add/{rId}";
    }

    private boolean isAdminLogged(HttpSession httpSession) {
        Object obj = httpSession.getAttribute("loggedAdmin");
        return obj != null && (Boolean) obj;
    }
}
