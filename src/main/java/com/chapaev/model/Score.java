package com.chapaev.model;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Chapaev on 09.06.2017.
 */
@Entity
@Table(name="score")
public class Score implements Serializable {
    @Id
    @Column(name="id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="restaurant_id")
    private Long restaurantId;

    @Column(name="name")
    private String customerName;

    @Column(name="customer_comment")
    private String customerComment;

    @Column(name="score_value")
    private Short customerScore;

    @Column(name="timestamp")
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm")
    private Date timestamp;

    public Long getId() {
        return id;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerComment() {
        return customerComment;
    }

    public void setCustomerComment(String customerComment) {
        this.customerComment = customerComment;
    }

    public Short getCustomerScore() {
        return customerScore;
    }

    public void setCustomerScore(Short customerScore) {
        this.customerScore = customerScore;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
