/**
 * Created by Chapaev on 09.06.2017.
 */
$().ready(function() {
    // validate registration form
    $.validator.addMethod("regex", function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Palun kontrollige üle sisendid!"
    );

    $('.score-validator').validate({
        rules: {
            customerName: {
                required: true,
                minlength: 1,
                maxlength: 64
            },
            customerComment: {
                required: true,
                minlength: 1,
                maxlength: 512
            },
            customerScore: {
                required: true,
                min: 1,
                max: 10,
                number: true
            }
        },
        messages: {
            customerName: "Palun sisestage oma nime!",
            customerComment: "Palun sisestage oma kommentaari!",
            customerScore: "Palun valige hinne!"
        }
    });

    $('.restaurant-validator').validate({
        rules: {
            restaurantName: {
                required: true,
                minlength: 1,
                maxlength: 128
            },
            restaurantAddress: {
                required: true,
                minlength: 1,
                maxlength: 256
            }
        },
        messages: {
            restaurantName: "Palun sisestage restorani nime!",
            restaurantAddress: "Palun sisestage restorani aadressi!"
        }
    });

});