<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../fragments/head.jsp"/>
<body>
<jsp:include page="../fragments/menu.jsp"/>
<div id="body">
    <!-- Main page content -->

    <section class="container">
        <div class="wrapper" style="transform: translateY(100%);">
            <form:form commandName="account" class="form-horizontal" method="post">
                <div class="form-group">
                    <label for="inputName" class="col-sm-3 col-sm-offset-1 control-label">Nimi</label>
                    <div class="col-sm-3 col-sm-offset-1">
                        <form:input path="name" type="text" class="form-control" id="inputName" placeholder="Nimi" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword" class="col-sm-3 col-sm-offset-1 control-label">Parool</label>
                    <div class="col-sm-3 col-sm-offset-1">
                        <form:input path="password" type="password" class="form-control" id="inputPassword" placeholder="Parool" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-5 col-sm-1">
                        <button type="submit" class="btn btn-default">Logi sisse</button>
                    </div>
                </div>
            </form:form>
        </div>
    </section>
</div>
<jsp:include page="../fragments/footer.jsp"/>
</body>
</html>
