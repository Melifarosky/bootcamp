<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../fragments/head.jsp"/>
<body>
<jsp:include page="../fragments/menu.jsp"/>
<div id="body">
    <!-- Main page content -->

    <section class="container">
        <div class="row">
            <h1>Valige asutus mida hinnata</h1>
        </div>
        <!--<div class="row">
            <div class="col-md-offset-9">
                <div class="input-group col-md-12">
                    <input type="text" id="search-input" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" onclick="findRestaurant()" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>-->
        <div class="row">
            <table id="restaurantView" class="table table-striped table-bordered table-hover selectable">
                <thead>
                <tr>
                    <th>Nimi</th>
                    <th>Asukoht</th>
                    <th>Keskmine hinne</th>
                    <th>Hinnatud (korda)</th>
                    <c:if test="${admin}"><th>Tegevused</th></c:if>
                </tr>
                </thead>
                <tbody>
                    <c:forEach items='${restaurants}' var='restaurant'>
                        <tr id='restaurant-${restaurant.id}' data-id='${restaurant.id}' onclick='addScore(${restaurant.id})'>
                            <td>${restaurant.restaurantName}</td>
                            <td>${restaurant.restaurantAddress}</td>
                            <td>${restaurant.averageScore != null ? restaurant.averageScore : 'Puudub'}</td>
                            <td>${restaurant.scoreCount}</td>
                            <c:if test="${admin}"><td style="width: 1px;">
                                <div class="restaurant-action-btns">
                                    <a href="restaurant/edit/${restaurant.id}" class="btn btn-success" role="button">Muuda</a>
                                    <form action="restaurant/delete/${restaurant.id}" method="post">
                                        <button class="btn btn-danger" role="button">Kustuta</button>
                                    </form>
                                </div>
                            </td></c:if>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <c:if test="${admin}">
            <div class="row">
                <a href="restaurant/new" class="btn btn-info">Lisa uus asutus</a>
            </div>
        </c:if>
    </section>
</div>
<jsp:include page="../fragments/footer.jsp"/>
<script>
    function addScore(restaurantId) {
        window.location = '/add/' + restaurantId;
    }
</script>
</body>
</html>
